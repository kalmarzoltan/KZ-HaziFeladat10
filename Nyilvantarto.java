package hazi_10;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class Nyilvantarto extends JFrame {

	private JPanel contentPane;
	private JList<Alkalmazott> jList;
	private DefaultListModel<Alkalmazott> defListmodel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Nyilvantarto frame = new Nyilvantarto();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Nyilvantarto() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		defListmodel = new DefaultListModel<Alkalmazott>();

		jList = new JList<Alkalmazott>();
		jList.setBounds(30, 70, 150, 180);
		contentPane.add(jList);

		Vezerlo v = new Vezerlo(new File(
				"C:\\Users\\Kalmi\\Documents\\eclipse-workspace\\Soter2.0\\src\\hazi_10\\000_Feladatok_GUI_data.txt"),
				defListmodel);

		defListmodel = v.readAlkalmazottatFromFile();
		jList.setModel(defListmodel);

		JLabel lblAlkalmazottakListja = new JLabel("Alkalmazottak listaja");
		lblAlkalmazottakListja.setBounds(30, 45, 150, 14);
		contentPane.add(lblAlkalmazottakListja);

		JLabel lblAlkalmazottNyilvntart = new JLabel("Alkalmazott nyilvantarto");
		lblAlkalmazottNyilvntart.setFont(new Font("Arial", Font.PLAIN, 14));
		lblAlkalmazottNyilvntart.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlkalmazottNyilvntart.setBounds(30, 11, 394, 23);
		contentPane.add(lblAlkalmazottNyilvntart);

		JButton btnHozzaad = new JButton("Hozzaadas");
		btnHozzaad.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AlkalmazottDialog ad = new AlkalmazottDialog(defListmodel, jList.getSelectedIndex());
				ad.setVisible(true);

			}
		});
		btnHozzaad.setBounds(190, 67, 89, 23);
		contentPane.add(btnHozzaad);

		JButton btnModosit = new JButton("Modositas");
		btnModosit.setBounds(190, 101, 89, 23);
		contentPane.add(btnModosit);
		btnModosit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AlkalmazottDialog ad = new AlkalmazottDialog(defListmodel, jList.getSelectedIndex());
				ad.setVisible(true);

			}
		});

		JButton btnTorol = new JButton("Torles");
		btnTorol.setBounds(190, 135, 89, 23);
		contentPane.add(btnTorol);
		btnTorol.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Torles: " + jList.getSelectedIndex());

				defListmodel.removeElementAt(jList.getSelectedIndex());

				PrintWriter pw = null;
				try {
					pw = new PrintWriter(
							"C:\\Users\\Kalmi\\Documents\\eclipse-workspace\\Soter2.0\\src\\hazi_10\\000_Feladatok_GUI_data.txt");
					for (int i = 0; i < defListmodel.getSize(); i++) {
						pw.println(defListmodel.getElementAt(i).getNev() + ";"
								+ defListmodel.getElementAt(i).getSzulDatum() + ";"
								+ defListmodel.getElementAt(i).getMunkaKezdDatum() + ";"
								+ defListmodel.getElementAt(i).getSzemIgSzam() + ";"
								+ defListmodel.getElementAt(i).getBeosztas());

					}

				} catch (FileNotFoundException ex) {
					JOptionPane.showMessageDialog(null, ex, "Hiba keletkezett!", JOptionPane.ERROR_MESSAGE);
				} finally {
					pw.close();
				}

			}
		});

	}

}
