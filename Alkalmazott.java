package hazi_10;

public class Alkalmazott {

	private String nev;
	private String szulDatum;
	private String munkaKezdDatum;
	private String szemIgSzam;
	private String beosztas;

	public Alkalmazott(String nev, String szulDatum, String munkaKezdDatum, String szemIgSzam, String beosztas) {
		this.nev = nev;
		this.szulDatum = szulDatum;
		this.munkaKezdDatum = munkaKezdDatum;
		this.szemIgSzam = szemIgSzam;
		this.beosztas = beosztas;
	}

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public String getSzulDatum() {
		return szulDatum;
	}

	public void setSzulDatum(String szulDatum) {
		this.szulDatum = szulDatum;
	}

	public String getMunkaKezdDatum() {
		return munkaKezdDatum;
	}

	public void setMunkaKezdDatum(String munkaKezdDatum) {
		this.munkaKezdDatum = munkaKezdDatum;
	}

	public String getSzemIgSzam() {
		return szemIgSzam;
	}

	public void setSzemIgSzam(String szemIgSzam) {
		this.szemIgSzam = szemIgSzam;
	}

	public String getBeosztas() {
		return beosztas;
	}

	public void setBeosztas(String beosztas) {
		this.beosztas = beosztas;
	}

	@Override
	public String toString() {
		return this.nev;
	}

}
