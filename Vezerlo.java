package hazi_10;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.DefaultListModel;

public class Vezerlo {

	private File file;

	private DefaultListModel defaultListModel;

	public Vezerlo(File file, DefaultListModel<Alkalmazott> defaultListModel) {
		this.file = file;
		this.defaultListModel = new DefaultListModel<>();
	}

	public DefaultListModel<Alkalmazott> readAlkalmazottatFromFile() {

		try {

			BufferedReader br = new BufferedReader(new FileReader(file));

			String sor;

			while ((sor = br.readLine()) != null) {

				Alkalmazott alkalmazott = new Alkalmazott(sor.split(";")[0], sor.split(";")[1], sor.split(";")[2],
						sor.split(";")[3], sor.split(";")[4]);
				defaultListModel.addElement(alkalmazott);

			}

		} catch (FileNotFoundException e) {
			System.err.println("A file nem talalhato!\n" + e.getMessage());

		} catch (IOException e) {
			System.err.println("A file nem olvashato!\n" + e.getMessage());

		}
		return defaultListModel;
	}

	public DefaultListModel<Alkalmazott> getListModel() {
		return getListModel();
	}

	public void setListModel(DefaultListModel<Alkalmazott> listModel) {
		this.defaultListModel = listModel;
	}

}
