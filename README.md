# KZ-HaziFeladat10
# ("Nyilvántartó")

### Repository:
<https://gitlab.com/kalmarzoltan/KZ-HaziFeladat10>




1. JButton btnHozzaad
```java
@Override
					public void actionPerformed(ActionEvent e) {

						if (selectedIndex == -1) {

							Alkalmazott alkalmazott = new Alkalmazott(tfNev.getText(), tfSzulDatum.getText(),
									tfMunkKezdDatum.getText(), tfSzemIgSzam.getText(), tfBeosztas.getText());

							listmodel.addElement(alkalmazott);
              ...
```
2. JButton btnModosit
```java
} else {

							System.out.println("selected for modification: " + selectedIndex);

							// Nehogy valamit átírjunk amit nem látunk -> nincs new Alkalmazott
							listmodel.get(selectedIndex).setNev(tfNev.getText());
							listmodel.get(selectedIndex).setSzulDatum(tfSzulDatum.getText());
							listmodel.get(selectedIndex).setMunkaKezdDatum(tfMunkKezdDatum.getText());
							listmodel.get(selectedIndex).setSzemIgSzam(tfSzemIgSzam.getText());
							listmodel.get(selectedIndex).setBeosztas(tfBeosztas.getText());
              ...
```
3. JButton btnTorol
```java
@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Torles: " + jList.getSelectedIndex());

				defListmodel.removeElementAt(jList.getSelectedIndex());
        ...
```
