package hazi_10;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class AlkalmazottDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField tfNev;
	private JTextField tfSzulDatum;
	private JTextField tfMunkKezdDatum;
	private JTextField tfSzemIgSzam;
	private JTextField tfBeosztas;
	private DefaultListModel<Alkalmazott> defaultListModel;

	/**
	 * Create the dialog.
	 */
	public AlkalmazottDialog(DefaultListModel<Alkalmazott> listmodel, Integer selectedIndex) {

		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		this.defaultListModel = listmodel;

		{
			JLabel lblAlkalmazottFelvtele = new JLabel("Alkalmazott felvetele");
			lblAlkalmazottFelvtele.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblAlkalmazottFelvtele.setHorizontalAlignment(SwingConstants.CENTER);
			lblAlkalmazottFelvtele.setBounds(10, 11, 414, 20);
			contentPanel.add(lblAlkalmazottFelvtele);
		}
		{
			JLabel lblNv = new JLabel("Nev:");
			lblNv.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNv.setBounds(58, 60, 125, 14);
			contentPanel.add(lblNv);
		}
		{
			JLabel lblSzletsiv = new JLabel("Szuletesi datum:");
			lblSzletsiv.setHorizontalAlignment(SwingConstants.RIGHT);
			lblSzletsiv.setBounds(68, 85, 115, 14);
			contentPanel.add(lblSzletsiv);
		}
		{
			JLabel lblMunkakezDtum = new JLabel("Munkakezdes datum:");
			lblMunkakezDtum.setHorizontalAlignment(SwingConstants.RIGHT);
			lblMunkakezDtum.setBounds(58, 110, 125, 14);
			contentPanel.add(lblMunkakezDtum);
		}
		{
			JLabel lblSzemIgSzm = new JLabel("Szem. Ig. Szam:");
			lblSzemIgSzm.setHorizontalAlignment(SwingConstants.RIGHT);
			lblSzemIgSzm.setBounds(58, 135, 125, 14);
			contentPanel.add(lblSzemIgSzm);
		}
		{
			JLabel lblBeoszts = new JLabel("Beosztas:");
			lblBeoszts.setHorizontalAlignment(SwingConstants.RIGHT);
			lblBeoszts.setBounds(58, 160, 125, 14);
			contentPanel.add(lblBeoszts);
		}

		tfNev = new JTextField();
		tfNev.setBounds(210, 57, 144, 20);
		contentPanel.add(tfNev);
		tfNev.setColumns(10);

		tfSzulDatum = new JTextField();
		tfSzulDatum.setColumns(10);
		tfSzulDatum.setBounds(210, 82, 144, 20);
		contentPanel.add(tfSzulDatum);

		tfMunkKezdDatum = new JTextField();
		tfMunkKezdDatum.setColumns(10);
		tfMunkKezdDatum.setBounds(210, 107, 144, 20);
		contentPanel.add(tfMunkKezdDatum);

		tfSzemIgSzam = new JTextField();
		tfSzemIgSzam.setColumns(10);
		tfSzemIgSzam.setBounds(210, 132, 144, 20);
		contentPanel.add(tfSzemIgSzam);

		tfBeosztas = new JTextField();
		tfBeosztas.setColumns(10);
		tfBeosztas.setBounds(210, 157, 144, 20);
		contentPanel.add(tfBeosztas);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						if (selectedIndex == -1) {

							Alkalmazott alkalmazott = new Alkalmazott(tfNev.getText(), tfSzulDatum.getText(),
									tfMunkKezdDatum.getText(), tfSzemIgSzam.getText(), tfBeosztas.getText());

							listmodel.addElement(alkalmazott);

							PrintWriter pw = null;
							try {
								pw = new PrintWriter(
										"C:\\Users\\Kalmi\\Documents\\eclipse-workspace\\Soter2.0\\src\\hazi_10\\000_Feladatok_GUI_data.txt");
								for (int i = 0; i < listmodel.getSize(); i++) {
									pw.println(listmodel.getElementAt(i).getNev() + ";"
											+ listmodel.getElementAt(i).getSzulDatum() + ";"
											+ listmodel.getElementAt(i).getMunkaKezdDatum() + ";"
											+ listmodel.getElementAt(i).getSzemIgSzam() + ";"
											+ listmodel.getElementAt(i).getBeosztas());

								}

							} catch (FileNotFoundException ex) {
								JOptionPane.showMessageDialog(null, ex, "Hiba keletkezett!", JOptionPane.ERROR_MESSAGE);
							} finally {
								pw.close();
							}
						} else {

							System.out.println("selected for modification: " + selectedIndex);

							// Nehogy valamit átírjunk amit nem látunk -> nincs new Alkalmazott
							listmodel.get(selectedIndex).setNev(tfNev.getText());
							listmodel.get(selectedIndex).setSzulDatum(tfSzulDatum.getText());
							listmodel.get(selectedIndex).setMunkaKezdDatum(tfMunkKezdDatum.getText());
							listmodel.get(selectedIndex).setSzemIgSzam(tfSzemIgSzam.getText());
							listmodel.get(selectedIndex).setBeosztas(tfBeosztas.getText());

							PrintWriter pw = null;
							try {
								pw = new PrintWriter(
										"C:\\Users\\Kalmi\\Documents\\eclipse-workspace\\Soter2.0\\src\\hazi_10\\000_Feladatok_GUI_data.txt");
								for (int i = 0; i < listmodel.getSize(); i++) {
									pw.println(listmodel.getElementAt(i).getNev() + ";"
											+ listmodel.getElementAt(i).getSzulDatum() + ";"
											+ listmodel.getElementAt(i).getMunkaKezdDatum() + ";"
											+ listmodel.getElementAt(i).getSzemIgSzam() + ";"
											+ listmodel.getElementAt(i).getBeosztas());

								}

							} catch (FileNotFoundException ex) {
								JOptionPane.showMessageDialog(null, ex, "Hiba keletkezett!", JOptionPane.ERROR_MESSAGE);
							} finally {
								pw.close();
							}

						}
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
